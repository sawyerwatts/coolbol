
###############################################################################
Program: CobolParser
Authors: Sawyer Watts and Alexandra Hanson
Desc:    This file contains the CoolbolParser class, which extends Parser and
         will convert COolBOL files to COBOL files.


###############################################################################
Class: CobolParser
Desc:  This class extends Parser to convert COBOL files to COolBOL.


###############################################################################
Function: __init__
Desc:     Constructor. If args are supplied, pass them to self.reload.
Args:     args=None - A str list; for more, see the function header for
                      reload.

###############################################################################
Function: usage
Desc:     Print the usage of this class.
Returns:  True iff the usage is printed.


###############################################################################
Function: _changeVariableTypes
Desc:     This method will change the variables in the self._lines[i] to be
          new versions.
Args:     i - The int index of self._lines to process.
Returns:  True iff the line is successful updated.


###############################################################################
Function: _insideVoicesPlease
Desc:     This method will change the self._lines[i] to uppercase.
Args:     i - The int index of self._lines to process.
Returns:  True iff the line is successful updated.
