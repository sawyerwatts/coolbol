       IDENTIFICATION DIVISION.
       PROGRAM-ID. SIERPINSKI-TRIANGLE-PROGRAM.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SIERPINSKI.
           05 N              PIC 99.
           05 I              PIC 999.
           05 K              PIC 999.
           05 M              PIC 999.
           05 C              PIC 9(18).
           05 I-LIMIT        PIC 999.
           05 Q              PIC 9(18).
           05 R              PIC 9.
       PROCEDURE DIVISION.
       CONTROL-PARAGRAPH.
           MOVE 4 TO N.
           MULTIPLY N BY 4 GIVING I-LIMIT.
           SUBTRACT 1 FROM I-LIMIT.
           PERFORM SIERPINSKI-PARAGRAPH
           VARYING I FROM 0 BY 1 UNTIL I IS GREATER THAN I-LIMIT.
           STOP RUN.
       SIERPINSKI-PARAGRAPH.
           SUBTRACT I FROM I-LIMIT GIVING M.
           MULTIPLY M BY 2 GIVING M.
           PERFORM M TIMES,
           DISPLAY SPACE WITH NO ADVANCING,
           END-PERFORM.
           MOVE 1 TO C.
           PERFORM INNER-LOOP-PARAGRAPH
           VARYING K FROM 0 BY 1 UNTIL K IS GREATER THAN I.
           DISPLAY ' '.
       INNER-LOOP-PARAGRAPH.
           DIVIDE C BY 2 GIVING Q REMAINDER R.
           IF R IS EQUAL TO ZERO THEN DISPLAY '  * ' WITH NO ADVANCING.
           IF R IS NOT EQUAL TO ZERO THEN DISPLAY ' ' WITH NO ADVANCING.
           COMPUTE C = C * (I - K) / (K + 1).
