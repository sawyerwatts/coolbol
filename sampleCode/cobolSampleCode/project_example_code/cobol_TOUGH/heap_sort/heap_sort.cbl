       >>source format free
      *> THIS CODE IS DEDICATED TO THE PUBLIC DOMAIN
      *> THIS IS gnucobol 2.0
       IDENTIFICATION DIVISION.
       PROGRAM-ID. HEAPSORT.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       REPOSITORY. FUNCTION ALL INTRINSIC.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  FILLER.
           03  A PIC 99.
           03  A-START PIC 99.
           03  A-END PIC 99.
           03  A-PARENT PIC 99.
           03  A-CHILD PIC 99.
           03  A-SIBLING PIC 99.
           03  A-LIM PIC 99 VALUE 10.
           03  ARRAY-SWAP PIC 99.
           03  ARRAY OCCURS 10 PIC 99.
       PROCEDURE DIVISION.
       START-HEAPSORT.
        
      *> FILL THE ARRAY
           COMPUTE A = RANDOM(SECONDS-PAST-MIDNIGHT)
           PERFORM VARYING A FROM 1 BY 1 UNTIL A > A-LIM
               COMPUTE ARRAY(A) = RANDOM() * 100
           END-PERFORM
        
           PERFORM DISPLAY-ARRAY
           DISPLAY  SPACE 'INITIAL ARRAY'
        
      *>HEAPIFY THE ARRAY
           MOVE A-LIM TO A-END
           COMPUTE A-START = (A-LIM + 1) / 2
           PERFORM SIFT-DOWN VARYING A-START FROM A-START BY -1 UNTIL A-START = 0
        
           PERFORM DISPLAY-ARRAY
           DISPLAY SPACE 'HEAPIFIED'
        
      *> SORT THE ARRAY
           MOVE 1 TO A-START
           MOVE A-LIM TO A-END
           PERFORM UNTIL A-END = A-START
               MOVE ARRAY(A-END) TO ARRAY-SWAP
               MOVE ARRAY(A-START) TO ARRAY(A-END)
               MOVE ARRAY-SWAP TO ARRAY(A-START)
               SUBTRACT 1 FROM A-END
               PERFORM SIFT-DOWN
           END-PERFORM
        
           PERFORM DISPLAY-ARRAY
           DISPLAY SPACE 'SORTED'
        
           STOP RUN
           .
       SIFT-DOWN.
           MOVE A-START TO A-PARENT
           PERFORM UNTIL A-PARENT * 2 > A-END
               COMPUTE A-CHILD = A-PARENT * 2 
               COMPUTE A-SIBLING = A-CHILD + 1
               IF A-SIBLING <= A-END AND ARRAY(A-CHILD) < ARRAY(A-SIBLIN
      *> TAKE THE GREATER OF THE TWO
                   MOVE A-SIBLING TO A-CHILD
               END-IF
               IF A-CHILD <= A-END AND ARRAY(A-PARENT) < ARRAY(A-CHILD)
      *> THE CHILD IS GREATER THAN THE PARENT
                  MOVE ARRAY(A-CHILD) TO ARRAY-SWAP
                  MOVE ARRAY(A-PARENT) TO ARRAY(A-CHILD)
                  MOVE ARRAY-SWAP TO ARRAY(A-PARENT)
               END-IF
      *> CONTINUE DOWN THE TREE
               MOVE A-CHILD TO A-PARENT
           END-PERFORM
           .
       DISPLAY-ARRAY.
           PERFORM VARYING A FROM 1 BY 1 UNTIL A > A-LIM
               DISPLAY SPACE ARRAY(A) WITH NO ADVANCING
           END-PERFORM
           .
       END PROGRAM HEAPSORT.
