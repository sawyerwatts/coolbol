       IDENTIFICATION DIVISION.
       PROGRAM-ID.  LUHNTEST.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       data division.
       WORKING-STORAGE SECTION.
       01  inp-card.
         03  inp-card-ch      PIC x(01) OCCURS 20 TIMES.
       01  ws-result          PIC 9(01).
         88  pass-luhn-test             VALUE 0.
 
       PROCEDURE DIVISION.
           MOVE "49927398716"       to inp-card
           PERFORM test-card
           MOVE "49927398717"       to inp-card
           PERFORM test-card
           MOVE "1234567812345678"  to inp-card
           PERFORM test-card
           MOVE "1234567812345670"  to inp-card
           PERFORM test-card
           STOP RUN  
           .
       TEST-CARD.
           CALL "LUHN" USING inp-card, ws-result
           IF pass-luhn-test
             DISPLAY "input=" inp-card "pass"
           ELSE 
             DISPLAY "input=" inp-card "fail"
           .
 
       END PROGRAM LUHNTEST.
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  LUHN.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  maxlen           PIC 9(02) COMP VALUE 16.
       01  inplen           PIC 9(02) COMP VALUE 0.
       01  i                PIC 9(02) COMP VALUE 0.
       01  j                PIC 9(02) COMP VALUE 0.
       01  l                PIC 9(02) COMP VALUE 0.
       01  dw               PIC 9(02) COMP VALUE 0.
       01  ws-total         PIC 9(03) COMP VALUE 0.
       01  ws-prod          PIC 99.
       01  FILLER REDEFINES ws-prod.
         03  ws-prod-tens   PIC 9.
         03  ws-prod-units  PIC 9.
       01  ws-card.
         03  FILLER OCCURS 16 times DEPENDING on maxlen.
           05  ws-card-ch     PIC x(01).
           05  ws-card-digit REDEFINES ws-card-ch  PIC 9(01).
       LINKAGE SECTION.
       01  inp-card.
         03  inp-card-ch      PIC x(01) OCCURS 20 TIMES.
       01  ws-result          PIC 9(01).
         88  pass-luhn-test             VALUE 0.
 
       PROCEDURE DIVISION using inp-card, ws-result.
           PERFORM VARYING i FROM 1 BY +1
           UNTIL i > maxlen
           OR inp-card-ch (i) = space
           END-PERFORM
           COMPUTE l = i - 1
           COMPUTE inplen = l
           PERFORM VARYING j from 1 by +1
           UNTIL j > inplen
             IF l < 1
               MOVE "0"             to ws-card-ch (j)
             ELSE 
               MOVE inp-card-ch (l) to ws-card-ch (j)
               COMPUTE l = l - 1
             END-IF
           END-PERFORM
           MOVE 0 to ws-total
           PERFORM VARYING i FROM 1 by +1
           UNTIL i > inplen
             COMPUTE dw = 2 - (i - 2 * function integer (i / 2))
             COMPUTE ws-prod = ws-card-digit (i) * dw
             COMPUTE ws-total = ws-total
                              + ws-prod-tens
                              + ws-prod-units
           END-PERFORM
           COMPUTE ws-result = ws-total - 10 * function integer (ws-total / 10)
           GOBACK 
           .
       END PROGRAM LUHN.
