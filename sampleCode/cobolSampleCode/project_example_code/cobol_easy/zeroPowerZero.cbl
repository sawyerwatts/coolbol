       IDENTIFICATION DIVISION.
       PROGRAM-ID. zero-power-zero-program. 
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       77 N                       PIC 9. 
       PROCEDURE DIVISION. 
            COMPUTE N = 0**0. 
            DISPLAY N UPON CONSOLE. 
            STOP RUN.
