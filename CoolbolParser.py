"""
Program: CoolbolParser
Authors: Sawyer Watts and Alexandra Hanson
Desc:    This file contains the CoolbolParser class, which extends Parser and
         will convert COolBOL files to COBOL files.
"""
import sys
from Parser import Parser

"""
Class: CoolbolParser
Desc:  This class extends Parser to convert COolBOL files to COBOL.
"""
class CoolbolParser(Parser):
    """
    Function: __init__
    Desc:     Constructor. If args are supplied, pass them to self.reload.
    Args:     args=None - A str list; for more, see the function header for
                          reload.
    """
    def __init__(self, args=None):
        # Initialize instance.
        super().__init__(".cbl")
        self.reset()

        # If appropriate, load the instance with argv data.
        if args is not None:
            self.reload(args)
 
    """
    Function: usage
    Desc:     Print the usage of this class.
    """
    def usage(self):
        usageList = [
                "USAGE: python3 CoolbolParser.py FILE [SWITCHES]",
                "",
                "DESC: Convert the passed COolBOL file to COBOL with changes specified by SWITCHES used."
                ]
        super()._usage(usageList)

    """
    Function: _changeVariableTypes
    Desc:     This method will change the variables in the self._lines[i] to be
              traditional COBOL syntax.
    Args:     i - The int index of self._lines to process. 
    """
    def _changeVariableTypes(self, i):
        old = self._lines[i]
        new = []
        picInserted = False

        # split the "old" line by spaces
        words = old.split()

        #iterate through each word in the old line
        for w in words:
            # If we have a variable type, then we have found a PIC clause.
            # Append the PIC clause and note that with the picInserted var.
            if (picInserted == False) and (w == "num" or w == "alphabetic" or w == "alphanumeric" or w == "imp-dec" or w == "signed" or w == "as-dec"):
                new.append("PIC ")
                picInserted = True

            # Replace the variable type with the appropriate COBOL syntax.
            # These are in-line with the previous if-statement since there may
            # be multiple types per line.
            if "num" in w:
                # Numeric
                new.append("9")
            elif "alphabetic" in w:
                # Alphabetic
                new.append("A")
            elif "alphanumeric" in w:
                # Alphanumeric
                new.append("X")
            elif "imp-dec" in w:
                # Implicit decimal
                new.append("V")
            elif "signed" in w:
                # A sign (+ or -)
                new.append("S")
            elif "as-dec" in w:
                # Assumed decimal
                new.append("p")

            # Check for " = ", parentheses (no space should be inserted back
            # in), period ".", and other
            elif w == "=":
                new.append(" VALUE ")
            elif len(w) > 0 and w[0] == "(":
                new.append(w)
            else:
                new.append(w)
                if "." not in w:
                    new.append(" ")

        self._lines[i] = "".join(new)
        return True

    """
    Function: _insideVoicesPlease
    Desc:     This method will change the self._lines[i] to lowercase.
    Args:     i - The int index of self._lines to process
    """
    def _insideVoicesPlease(self, i):
        # The documentation found says that COBOL only uses ' or " for strings.
        old = self._lines[i]
        new = []
        isString = False
        lastWord = old.split(" ")[-1]
        if lastWord == "SECTION." or lastWord == "DIVISION.":
            return True

        for char in old:
            if char == '"' or char == "'":
                isString = not isString

            if isString:
                new.append(char)
            else:
                new.append(char.upper())

        self._lines[i] = "".join(new)
        return True


###############################################################################
if __name__ == "__main__":
    myParser = CoolbolParser(sys.argv[1:])
    if not myParser.usage():
        if not myParser.convert():
            print("ERROR: some failure occurred.")


