"""
Program: CobolParser
Authors: Sawyer Watts and Alexandra Hanson
Desc:    This file contains the CoolbolParser class, which extends Parser and
         will convert COolBOL files to COBOL files.
"""
import sys
from Parser import Parser

"""
Class: CobolParser
Desc:  This class extends Parser to convert COBOL files to COolBOL.
"""
class CobolParser(Parser):
    """
    Function: __init__
    Desc:     Constructor. If args are supplied, pass them to self.reload.
    Args:     args=None - A str list; for more, see the function header for
                          reload.
    """
    def __init__(self, args=None):
        # Initialize instance.
        super().__init__(".coolbol")
        self.reset()

        # If appropriate, load the instance with argv data.
        if args is not None:
            self.reload(args)

    """
    Function: usage
    Desc:     Print the usage of this class.
    Returns:  True iff the usage is printed.
    """
    def usage(self):
        usageList = [
                "USAGE: python3 CobolParser.py FILE [SWITCHES]",
                "",
                "DESC: Convert the passed COBOL file to COolBOL with changes specified by SWITCHES used."
                ]
        return super()._usage(usageList)

    """
    Function: _changeVariableTypes
    Desc:     This method will change the variables in the self._lines[i] to be
              new versions.
    Args:     i - The int index of self._lines to process.
    Returns:  True iff the line is successful updated.
    """
    def _changeVariableTypes(self, i):
        old = self._lines[i]
        new = []
        isPic = False

        # split the "old" line by spaces 
        words = old.split()

        #iterate through each word in the old line 
        for w in words:
            if isPic:
                # If the previous word was "PIC", the current word is the variable 
                # type declaration. Iterate through the characters in that word to 
                # check what kind of variable it is.
                isExpression = False
                for char in w:
                    # Determine if we are in the "expression" -- this
                    # is the number of bytes used by the data item
                    if char == ")":
                        isExpression = False
                    elif char == "(":
                        isExpression = True

                    if isExpression == True:
                    # If we are in the "expression," append the character
                        new.append(char)
                    else:
                    # If we are not in the "expression," check what type of
                    # data we have by the current character. These guards also
                    # check if we have reached the end of the sentence with "."
                    # and other cases, i.e. if we have ")"
                        if char.lower() == "9":
                            # Numeric
                            new.append("num")
                            new.append(" ")
                        elif char.lower() == "a":
                            # Alphabetic
                            new.append("alphabetic")
                            new.append(" ")
                        elif char.lower() == "x":
                            # Alphanumeric
                            new.append("alphanumeric")
                            new.append(" ")
                        elif char.lower() == "v":
                            # Implicit decimal
                            new.append("imp-dec")
                            new.append(" ")
                        elif char.lower() == "s":
                            # A sign (+ or -)
                            new.append("signed")
                            new.append(" ")
                        elif char.lower() == "p":
                            # Assumed decimal
                            new.append("as-dec")
                            new.append(" ")
                        elif char.lower() == ".":
                            # End of sentence
                            # Delete last space and append current char.
                            new = new[:-1]
                            new.append(char)
                        else:
                            # Otherwise add the character follow by a space
                            new.append(char)
                            new.append(" ")
                # We have iterated through the data type, set isPic back to
                # false
                isPic = False
            elif w.lower() == "pic":
                # If the current word is PIC, set isPic to true 
                isPic = True
            elif w.lower() == "value":
                # If we have value clause, convert to " = " symbol.
                new.append("= ")
            else:
                # otherwise append the old word and add the space back in
                new.append(w + " ")

        self._lines[i] = "".join(new)[:-1]
        return True

    """
    Function: _insideVoicesPlease
    Desc:     This method will change the self._lines[i] to uppercase.
    Args:     i - The int index of self._lines to process.
    Returns:  True iff the line is successful updated.
    """
    def _insideVoicesPlease(self, i):
        # The documentation found says that COBOL only uses ' or " for strings.
        old = self._lines[i]
        new = []
        isString = False
        lastWord = old.split(" ")[-1]
        if lastWord == "SECTION." or lastWord == "DIVISION.":
            return True

        for char in old:
            if char == '"' or char == "'":
                isString = not isString

            if isString:
                new.append(char)
            else:
                new.append(char.lower())

        self._lines[i] = "".join(new)
        return True


###############################################################################
if __name__ == "__main__":
    myParser = CobolParser(sys.argv[1:])
    if not myParser.usage():
        if not myParser.convert():
            print("ERROR: some failure occurred.")


