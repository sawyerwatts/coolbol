# COolBOL: Term Project for CS410/510: Code Reading and Review

Write a wrapper for the COBOL language to tweak certain aspects of the language to see if it improves readability.
This parser can convert to and from COBOL and COolBOL.

## Features
### Switch: --inside-voices-please
This converts non-strings, non-division sentences, and non-section sentences to lower case.

### Switch: --change-variable-types
This converts the characters that represent data types to more descriptive words.

## Installation
Clone this repo then clone the submodules with `git submodule update --init`
