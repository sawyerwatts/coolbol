"""
Program: Parser
Authors: Sawyer Watts and Alexandra Hanson
Desc:    This file contains the Parser class, which will extend CommandReader
         and serve as an 'abstract' super class for CoolbolParser and
         CobolParser.
"""
import sys
from commandreader import CommandReader

"""
Class: Parser
Desc:  'Abstract.' This class extends CommandReader and serve as a base class
       to the different parsers.
"""
class Parser(CommandReader):

    """
    Function: __init__
    Desc:     Constructor. If args are supplied, pass them to self.reload.
    Args:     fileExtension - The str of the file extension that the class will
                              write the results with.
              args=None - A str list; for more, see the function header for
                          reload.
    """
    def __init__(self, fileExtension, args=None):
        # Initialize instance.
        super().__init__()
        self.reset()
        self._fileExtension = fileExtension

        # Adjust valid switches/options.
        del self._validOptions["-f"]
        self._validSwitches["--inside-voices-please"] = ["Adjust the case of all non-string, all non-division sentences, and all non-section sentences to/from lowercase, depending on the parser."]
        self._validSwitches["--change-variable-types"] = ["Adjust how variable types are signified."]

        # If appropriate, load the instance with argv data.
        if args is not None:
            self.reload(args)

    """
    Function: reset
    Desc:     Reset this instance to the a freshly constructed state.
    """
    def reset(self):
        super().reset()
        self._lines = list()

    """
    Function: _readFile
    Desc:     This method will read the file, stored in -S, and store each line
              into self._lines for later use.
    Returns:  True iff the file is successfully written to self._lines.
    """
    def _readFile(self):
        if "-S" not in self._inputs:
            return False

        # CommandReader is set to be able to store several -S values, but
        # Parser only cares about working on a single file.
        fileName = self._inputs["-S"].getValues(0)

        # Option.getValues will return an empty list on failure, so check for
        # that.
        if isinstance(fileName, list):
            return False

        inputFile = None

        try:
            inputFile = open(fileName, "r")
        except FileNotFoundError as err:
            print("WARNING: An invalid file '" + fileName + "' has been entered.")
            return False

        for line in inputFile.readlines():
            self._lines.append(line.replace("\n", ""))

        return True

    """
    Function: _writeFile
    Desc:     This method will write the lines of text stored in self._lines to
              a file of the same name as the file in -S but with the stored
              file extension appended.
    Returns:  True iff self._lines is successfully written to the file in -S.
    """
    def _writeFile(self):
        if "-S" not in self._inputs:
            return False

        # CommandReader is set to be able to store several -S values, but
        # Parser only cares about working on a single file.
        fileName = self._inputs["-S"].getValues(0)

        # Option.getValues() will return an empty list on failure, so check for
        # that.
        if isinstance(fileName, list):
            return False

        # TODO: have this remove the extension from the old file, if present.
        fileName = "".join([fileName, self._fileExtension])
        outputFile = open(fileName, "w")
        for line in self._lines:
            outputFile.write(line)
            outputFile.write("\n")

        return True

    """
    Function: convert
    Desc:     This method will read the file (if no lines stored), convert the
              COBOL to or from COolBOL, and write the results to a file with
              the same name but with a ".coolbol" or ".cbl" extension, as
              appropriate.
    Returns:  True iff a file is written.
    """
    def convert(self):
        # Gather the lines.
        if len(self._lines) == 0:
            if not self._readFile():
                return False

        # Loop through and process the lines.
        for i in range(len(self._lines)):
            # NOTE: this is not a very OOP way of processing the loaded data.
            # A better way would be to extend Switch for a class that contains
            # an invocation method, and then this class will be extended for
            # ChangeVariableTypes and InsideVoicesPlease. Then update
            # CommandReader.addSwitch() to load these instances into the
            # corresponding value of CommandReader._inputs. Here, instead of
            # hardcoding the switches, loop through self._inputs and call the
            # universal Switch method for each loaded value. This method would
            # need to know if it should convert to lower or upper case.
            if "--change-variable-types" in self._inputs:
                if not self._changeVariableTypes(i):
                    return False

            if "--inside-voices-please" in self._inputs:
                if not self._insideVoicesPlease(i):
                    return False

        # Write the lines.
        if not self._writeFile():
            return False

        return True

    """
    Function: _changeVariableTypes
    Desc:     This method will change the variables in the self._lines[i] to be
              different, or revert them back to defaults.
              NOTE: this is 'abstract' and should be implemented by subclasses.
    Args:     i - The int index of self._lines to process.
    Returns:  True iff the line is successful updated.
    Throws:   NameError - Always thrown because this class is 'abstract.'
    """
    def _changeVariableTypes(self, i):
        raise NameError("Parser._changeVariableTypes() needs to be implemented.")

    """
    Function: _insideVoicesPlease
    Desc:     This method will change the self._lines[i] to/from uppercase.
              NOTE: this is 'abstract' and should be implemented by subclasses.
    Args:     i - The int index of self._lines to process.
    Returns:  True iff the line is successful updated.
    Throws:   NameError - Always thrown because this class is 'abstract.'
    """
    def _insideVoicesPlease(self, i):
        raise NameError("Parser._insideVoicesPlease() needs to be implemented.")

    """
    Function: usage
    Desc:     Print the usage of this class.
    Returns:  True iff the usage is printed.
    """
    def usage(self):
        usageList = [
                "USAGE: python3 Parser.py FILE [SWITCHES]",
                "",
                "DESC: Act as an intermediate clsas between CommandReader and the COBOL/COolBOL to/from parsers."
                ]
        return super()._usage(usageList)


###############################################################################
# "Main"
if __name__ == "__main__":
    myParser = Parser(".FAKE", sys.argv[1:])
    myParser.usage()

